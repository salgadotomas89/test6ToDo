package com.nisum;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.nisum.dao.ToDoDao;
import com.nisum.model.ToDo;
import com.nisum.service.ToDoService;

@RunWith(MockitoJUnitRunner.class)
public class ToDoServiceTest {

	@Mock
	private ToDoDao toDo;
	
	@InjectMocks
	private ToDoService toDoService;
	
	@Test
	public void buscarLibroPorId(){
		//arrange
		ToDo nuevaTarea = new ToDo();
		nuevaTarea.setId(11111);
		//Act
		when(toDo.findOne(anyLong())).thenReturn(nuevaTarea);
		ToDo tareaCreada = toDoService.obtenerTareaPorHacer(nuevaTarea.getId());
		//Assert
		Assert.assertNotNull(tareaCreada);
		Assert.assertEquals(nuevaTarea,tareaCreada);
		
	}
}
