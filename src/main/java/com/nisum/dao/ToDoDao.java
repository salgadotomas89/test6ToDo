package com.nisum.dao;
import org.springframework.data.repository.CrudRepository;

import com.nisum.model.ToDo;

public interface ToDoDao extends CrudRepository<ToDo, Long>{
	public ToDo findBy(Long id);
}
