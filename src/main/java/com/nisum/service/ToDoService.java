package com.nisum.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.nisum.dao.ToDoDao;
import com.nisum.model.ToDo;

public class ToDoService {
	
	@Autowired
	private ToDoDao dao;

	public ToDo obtenerTareaPorHacer(long id) {
		return dao.findOne(id);
	}

}
